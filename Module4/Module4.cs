﻿using System;

namespace M4
{
    public class Module4
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello world");
        }


        public int Task_1_A(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "Entered array is empty");
            }
            int maxElement = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] >= maxElement) maxElement = array[i];

            }
            return maxElement;
        }

        public int Task_1_B(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "Entered array is empty");
            }
            int minElement = array[0];
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < minElement) minElement = array[i];

            }
            return minElement;
        }

        public int Task_1_C(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "Entered array is empty");
            }
            int result = 0;

            for (int i = 0; i < array.Length; i++)
            {
                result += array[i];
            }

            return result;
        }

        public int Task_1_D(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "Entered array is empty");
            }

            int result = Task_1_A(array) - Task_1_B(array);

            return result;
        }

        public void Task_1_E(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "Entered array is empty");
            }
            int adding = Task_1_A(array);

            int subtraction = Task_1_B(array);

            Console.WriteLine(adding + " " + subtraction);

            for (int i = 0; i < array.Length; i++)
            {
                if (i % 2 == 0)
                {
                    array[i] += adding;
                }
                else
                {
                    array[i] -= subtraction;
                }
            }
        }

        public int Task_2(int a, int b, int c)
        {
            return a + b + c;
        }

        public int Task_2(int a, int b)
        {
            return a + b;
        }

        public double Task_2(double a, double b, double c)
        {
            return a + b + c;
        }

        public string Task_2(string a, string b)
        {
            return a + b;
        }

        public int[] Task_2(int[] a, int[] b)
        {
            if (a == null || a.Length == 0) 
            {
                throw new ArgumentNullException("a","Entered array is empty");
            }
            else if( b == null || b.Length == 0)
            {
                throw new ArgumentNullException("b", "Entered array is empty");
            }

            if (a.Length > b.Length)
            {
                for (int i = 0; i < b.Length; i++)
                {
                    a[i] += b[i];
                }
                return a;
            }
            else
            {
                for (int i = 0; i < a.Length; i++)
                {
                    b[i] += a[i];
                }
                return b;
            }
        }

        public void Task_3_A(ref int a, ref int b, ref int c)
        {
            a += 10;
            b += 10;
            c += 10;
        }

        public void Task_3_B(double radius, out double length, out double square)
        {
            if (radius < 0)
            {
                throw new ArgumentException( "Radius cant be negative", "radius");
            }
            length = 2 * Math.PI * radius;

            square = Math.PI * Math.Pow(radius, 2);
        }

        public void Task_3_C(int[] array, out int maxItem, out int minItem, out int sumOfItems)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("Entered array is empty" , "array");
            }
            maxItem = Task_1_A(array);
            minItem = Task_1_B(array);
            sumOfItems = Task_1_C(array);

        }

        public (int, int, int) Task_4_A((int, int, int) numbers)
        {
            numbers.Item1 += 10;
            numbers.Item2 += 10;
            numbers.Item3 += 10;

            return numbers;
        }

        public (double, double) Task_4_B(double radius)
        {
            if (radius < 0)
            {
                throw new ArgumentException("radius","Radius cant be negative");
            }

            (double, double) result;
            result.Item1 = 2 * Math.PI * radius;
            result.Item2 = Math.PI * Math.Pow(radius, 2);

            return result;
        }

        public (int, int, int) Task_4_C(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "Entered array is empty");
            }
            (int, int, int) result;
            result.Item1 = Task_1_B(array);
            result.Item2 = Task_1_A(array);
            result.Item3 = Task_1_C(array);

            return result;
        }

        public void Task_5(int[] array)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "Entered array is empty");
            }
            for (int i = 0; i < array.Length; i++)
            {
                array[i] += 5;
            }
        }

        public void Task_6(int[] array, SortDirection direction)
        {
            if (array == null || array.Length == 0)
            {
                throw new ArgumentNullException("array", "Entered array is empty");
            }

            if (direction == SortDirection.Ascending)
            {
                int minimalIndex;
                int minimalValue;
                int helper;
                for (int i = 0; i < array.Length; i++)
                {
                    minimalValue = array[i];
                    minimalIndex = i;

                    for (int j = i; j < array.Length; j++)
                    {
                        if (array[j] < minimalValue)
                        {
                            minimalIndex = j;
                            minimalValue = array[j];
                        }
                    }

                    helper = array[i];
                    array[i] = array[minimalIndex];
                    array[minimalIndex] = helper;
                }

            }
            else
            {
                int maxIndex;
                int maxValue;
                int helper;

                for (int i = 0; i < array.Length; i++)
                {
                    maxValue = array[i];
                    maxIndex = i;

                    for (int j = i; j < array.Length; j++)
                    {
                        if (array[j] > maxValue)
                        {
                            maxIndex = j;
                            maxValue = array[j];
                        }
                    }
                    helper = array[i];
                    array[i] = array[maxIndex];
                    array[maxIndex] = helper;
                }

            }
        }

        public double Task_7(Func<double, double> func, double x1, double x2, double e, double result = 0)
        {

            result = (x1 + x2) / 2;

            if (func(x1) * func(result) < 0)
            {
                x2 = result;
            }
            else if (func(result) * func(x2) < 0)
            {
                x1 = result;
            }
            if (Math.Abs(x2 - x1) > 2 * e)
            {
                return Task_7(func, x1, x2, e, result);
            }
            return result;
        }
    }
}
